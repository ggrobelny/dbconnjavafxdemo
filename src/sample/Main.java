package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import sample.Database.DBUtil;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Views/sample.fxml"));
        primaryStage.setTitle("DB Connection");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) throws Exception {

        Connection conn = DBUtil.getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.getResultSet();

        //String Command = "insert into Emp values(5, 'Noe', 100)";
        //int count = stm.executeUpdate(Command);

        //System.out.println(count + " Robi robotę");
        System.out.println(" Robi robotę");
        DBUtil.Close(rs);
        DBUtil.Close(stm);
        DBUtil.Close(conn);

        launch(args);
    }
}
